const std = @import("std");

pub fn build(b: *std.Build) !void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const lib = b.addStaticLibrary(.{
        .name = "keychain",
        .root_source_file = .{ .path = "src/keychain.zig" },
        .target = target,
        .optimize = optimize,
    });

    const os_tag = target.result.os.tag;

    if (os_tag.isDarwin()) {
        lib.linkFramework("Security");
        lib.linkFramework("CoreFoundation");
    } else if (os_tag == .windows) {
        lib.linkLibC();
    }

    _ = b.addModule("keychain", .{ .root_source_file = .{ .path = "./src/keychain.zig" }});
    // b.installArtifact(lib);

    const unit_tests = b.addTest(.{
        .root_source_file = .{ .path = "src/keychain_tests.zig" },
        .target = target,
        .optimize = optimize,
    });

    if (os_tag.isDarwin()) {
        unit_tests.linkFramework("Security");
        unit_tests.linkFramework("CoreFoundation");
    } else if (os_tag == .windows) {
        unit_tests.linkLibC();
    }

    const run_unit_tests = b.addRunArtifact(unit_tests);

    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_unit_tests.step);
}
