const std = @import("std");
const builtin = @import("builtin");

pub const KeychainError = error{ PlatformNotSupported, PasswordNotFound };

pub fn getPassword(allocator: std.mem.Allocator, package: []const u8, service: []const u8, user: []const u8) ![]const u8 {
    const os_tag = builtin.target.os.tag;

    if (os_tag.isDarwin()) {
        return @import("./macos/keychain.zig").getPassword(allocator, package, service, user);
    } else {
        switch (os_tag) {
            .windows => return @import("./windows/keychain.zig").getPassword(allocator, package, service, user),
            .linux => return @import("./linux/keychain.zig").getPassword(allocator, package, service, user),
            else => return KeychainError.PlatformNotSupported,
        }
    }
}

pub fn setPassword(allocator: std.mem.Allocator, package: []const u8, service: []const u8, user: []const u8, password: []const u8) !void {
    const os_tag = builtin.target.os.tag;

    if (os_tag.isDarwin()) {
        return @import("./macos/keychain.zig").setPassword(allocator, package, service, user, password);
    } else {
        switch (os_tag) {
            .windows => return @import("./windows/keychain.zig").setPassword(allocator, package, service, user, password),
            .linux => return @import("./linux/keychain.zig").setPassword(allocator, package, service, user, password),
            else => return KeychainError.PlatformNotSupported,
        }
    }
}

pub fn deletePassword(allocator: std.mem.Allocator, package: []const u8, service: []const u8, user: []const u8) !void {
    const os_tag = builtin.target.os.tag;

    if (os_tag.isDarwin()) {
        return @import("./macos/keychain.zig").deletePassword(allocator, package, service, user);
    } else {
        switch (os_tag) {
            .windows => return @import("./windows/keychain.zig").deletePassword(allocator, package, service, user),
            .linux => return @import("./linux/keychain.zig").deletePassword(allocator, package, service, user),
            else => return KeychainError.PlatformNotSupported,
        }
    }
}
