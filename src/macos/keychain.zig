const std = @import("std");
const apple = @import("apple.zig");

const service_format = "{s}.{s}";
const KeychainError = error{ NotYetImplemented, PasswordNotFound, PasswordTooLong, PasswordSaveFailed, PasswordDeleteFailed };

pub fn getPassword(allocator: std.mem.Allocator, package: []const u8, service: []const u8, user: []const u8) ![]const u8 {
    const service_name = try std.fmt.allocPrint(std.heap.page_allocator, service_format, .{ package, service });
    defer std.heap.page_allocator.free(service_name);

    const cf_dict: apple.CFMutableDictionaryRef = apple.CFDictionaryCreateMutable(
        apple.kCFAllocatorDefault,
        4,
        &apple.kCFTypeDictionaryKeyCallBacks,
        &apple.kCFTypeDictionaryValueCallBacks
    );

    const cf_user: apple.CFStringRef = apple.CFStringCreateWithCString(apple.kCFAllocatorDefault, user.ptr, apple.kCFStringEncodingUTF8);
    const cf_service: apple.CFStringRef = apple.CFStringCreateWithCString(apple.kCFAllocatorDefault, service_name.ptr, apple.kCFStringEncodingUTF8);

    apple.CFDictionaryAddValue(cf_dict, apple.kSecClass, apple.kSecClassGenericPassword);
    apple.CFDictionaryAddValue(cf_dict, apple.kSecAttrAccount, cf_user);
    apple.CFDictionaryAddValue(cf_dict, apple.kSecAttrService, cf_service);
    apple.CFDictionaryAddValue(cf_dict, apple.kSecReturnData, apple.kCFBooleanTrue);

    var password_data: apple.CFTypeRef = null;
    const find_result = apple.SecItemCopyMatching(cf_dict, &password_data);

    if (find_result != apple.noErr) {
        return KeychainError.PasswordNotFound;
    }
    
    const length: usize = @intCast(apple.CFDataGetLength(@ptrCast(password_data)));
    const password = try allocator.alloc(u8, length);
    apple.CFDataGetBytes(@ptrCast(password_data), apple.CFRangeMake(0, @intCast(length)), password.ptr);

    return password;
}

pub fn setPassword(allocator: std.mem.Allocator, package: []const u8, service: []const u8, user: []const u8, password: []const u8) !void {

    const service_name = try std.fmt.allocPrint(std.heap.page_allocator, service_format, .{ package, service });
    defer std.heap.page_allocator.free(service_name);

    const cf_dict: apple.CFMutableDictionaryRef = apple.CFDictionaryCreateMutable(
        apple.kCFAllocatorDefault,
        4,
        &apple.kCFTypeDictionaryKeyCallBacks,
        &apple.kCFTypeDictionaryValueCallBacks
    );

    const cf_user: apple.CFStringRef = apple.CFStringCreateWithCString(apple.kCFAllocatorDefault, user.ptr, apple.kCFStringEncodingUTF8);
    const cf_service: apple.CFStringRef = apple.CFStringCreateWithCString(apple.kCFAllocatorDefault, service_name.ptr, apple.kCFStringEncodingUTF8);
    const cf_password: apple.CFStringRef = apple.CFStringCreateWithCString(apple.kCFAllocatorDefault, password.ptr, apple.kCFStringEncodingUTF8);

    apple.CFDictionaryAddValue(cf_dict, apple.kSecClass, apple.kSecClassGenericPassword);
    apple.CFDictionaryAddValue(cf_dict, apple.kSecAttrAccount, cf_user);
    apple.CFDictionaryAddValue(cf_dict, apple.kSecAttrService, cf_service);
    apple.CFDictionaryAddValue(cf_dict, apple.kSecValueData, cf_password);

    var result = apple.SecItemAdd(cf_dict, null);

    if (result == apple.errSecDuplicateItem) {
        try deletePassword(allocator, package, service, user);
        result = apple.SecItemAdd(cf_dict, null);
    }
    
    if (result != apple.noErr) {
        return KeychainError.PasswordSaveFailed;
    }

    return;
}

pub fn deletePassword(allocator: std.mem.Allocator, package: []const u8, service: []const u8, user: []const u8) !void {
    _ = allocator;
    const service_name = try std.fmt.allocPrint(std.heap.page_allocator, service_format, .{ package, service });
    defer std.heap.page_allocator.free(service_name);

    const cf_dict: apple.CFMutableDictionaryRef = apple.CFDictionaryCreateMutable(
        apple.kCFAllocatorDefault,
        3,
        &apple.kCFTypeDictionaryKeyCallBacks,
        &apple.kCFTypeDictionaryValueCallBacks
    );

    const cf_user: apple.CFStringRef = apple.CFStringCreateWithCString(apple.kCFAllocatorDefault, user.ptr, apple.kCFStringEncodingUTF8);
    const cf_service: apple.CFStringRef = apple.CFStringCreateWithCString(apple.kCFAllocatorDefault, service_name.ptr, apple.kCFStringEncodingUTF8);

    apple.CFDictionaryAddValue(cf_dict, apple.kSecClass, apple.kSecClassGenericPassword);
    apple.CFDictionaryAddValue(cf_dict, apple.kSecAttrAccount, cf_user);
    apple.CFDictionaryAddValue(cf_dict, apple.kSecAttrService, cf_service);

    const result = apple.SecItemDelete(cf_dict);

    if (result != apple.noErr) {
        return KeychainError.PasswordDeleteFailed;
    }

    return;
}