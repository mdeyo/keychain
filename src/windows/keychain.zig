const std = @import("std");
const windows = @import("windows.zig");

const KeychainError = error{ PasswordNotFound, PasswordTooLong, PasswordSaveFailed, PasswordDeleteFailed };
const target_format = "{s}.{s}/{s}";

pub fn setPassword(allocator: std.mem.Allocator, package: []const u8, service: []const u8, user: []const u8, password: []const u8) !void {
    const target = try std.fmt.allocPrint(allocator, target_format, .{ package, service, user });
    defer allocator.free(target);

    // TODO: Figure out why any other allocator won't work here.
    const wide_alloc = std.heap.page_allocator;
    const wide_target = try std.unicode.utf8ToUtf16LeAlloc(wide_alloc, target);
    defer wide_alloc.free(wide_target);

    const wide_user = try std.unicode.utf8ToUtf16LeAlloc(wide_alloc, user);
    defer wide_alloc.free(wide_user);

    const wide_pass = try std.unicode.utf8ToUtf16LeAlloc(wide_alloc, password);
    defer wide_alloc.free(wide_pass);

    const wide_pass_length = try std.unicode.calcUtf16LeLen(password) * 2;

    if (wide_pass_length > windows.CRED_MAX_CREDENTIAL_BLOB_SIZE) {
        return KeychainError.PasswordTooLong;
    }

    var cred: windows.CREDENTIALW = .{
        .Type = windows.CRED_TYPE_GENERIC,
        .TargetName = wide_target.ptr,
        .UserName = wide_user.ptr,
        .CredentialBlobSize = @intCast(wide_pass_length),
        .CredentialBlob = @ptrCast(wide_pass.ptr),
        .Persist = windows.CRED_PERSIST_ENTERPRISE,
    };

    if (windows.CredWriteW(&cred, 0) == 0) {
        return KeychainError.PasswordSaveFailed;
    }

    return;
}

pub fn getPassword(
    allocator: std.mem.Allocator,
    package: []const u8,
    service: []const u8,
    user: []const u8,
) ![]const u8 {
    const target = try std.fmt.allocPrint(allocator, target_format, .{ package, service, user });
    defer allocator.free(target);

    // TODO: Figure out why any other allocator won't work here.
    const wide_target_alloc = std.heap.page_allocator;
    const wide_target = try std.unicode.utf8ToUtf16LeAlloc(wide_target_alloc, target);
    defer wide_target_alloc.free(wide_target);

    var cred_ptr: ?*windows.CREDENTIALW = undefined;
    defer windows.CredFree(cred_ptr);

    if (windows.CredReadW(wide_target.ptr, windows.CRED_TYPE_GENERIC, 0, &cred_ptr) == 0) {
        return KeychainError.PasswordNotFound;
    }

    const cred = cred_ptr.?;

    const utf16_length = cred.CredentialBlobSize / 2;
    const utf16_password = try allocator.alloc(u16, utf16_length);
    defer allocator.free(utf16_password);

    const u8_password = cred.CredentialBlob[0..cred.CredentialBlobSize];

    for (0..utf16_length) |index| {
        if (index * 2 < u8_password.len) {
            const byte1 = u8_password[index * 2];
            const byte2 = u8_password[index * 2 + 1];
            utf16_password[index] = @as(u16, byte1) | @as(u16, byte2) << 8;
        } else {
            utf16_password[index] = 0;
        }
    }

    return try std.unicode.utf16leToUtf8Alloc(allocator, utf16_password);
}

pub fn deletePassword(allocator: std.mem.Allocator, package: []const u8, service: []const u8, user: []const u8) !void {
    const target = try std.fmt.allocPrint(allocator, target_format, .{ package, service, user });
    defer allocator.free(target);

    // TODO: Figure out why any other allocator won't work here.
    const wide_target_alloc = std.heap.page_allocator;
    const wide_target = try std.unicode.utf8ToUtf16LeAlloc(wide_target_alloc, target);
    defer wide_target_alloc.free(wide_target);

    if (windows.CredDeleteW(wide_target.ptr, windows.CRED_TYPE_GENERIC, 0) == 0) {
        return KeychainError.PasswordDeleteFailed;
    }

    return;
}
