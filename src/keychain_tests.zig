const std = @import("std");
const keychain = @import("keychain.zig");

const alloc = std.testing.allocator;
const package = "com.madebymathew";
const service = "cldl";
const user = "test-user";

fn testPassword(password: []const u8) !void {
    try keychain.setPassword(alloc, package, service, user, password);

    const returned_pass = try keychain.getPassword(alloc, package, service, user);
    defer alloc.free(returned_pass);

    try std.testing.expectEqualStrings(password, returned_pass);

    try keychain.deletePassword(alloc, package, service, user);

    _ = keychain.getPassword(alloc, package, service, user) catch |err| {
        try std.testing.expect(keychain.KeychainError.PasswordNotFound == err);
    };
}

test "ascii_password" {
    try testPassword("ABCXYZabcxyz0123789.");
}

test "latin_supplement_password" {
    try testPassword("áéíóúüñçß");
}

test "symbols_password" {
    try testPassword("©®™€£¥§¶¿¡");
}

test "emoji_password" {
    try testPassword("😀🎉🌍🍎💡🚀📚");
}

test "supplement_password" {
    try testPassword("𐌀𠀀𪛕");
}
